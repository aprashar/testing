###INITIAL SETUP to set ssh of device on remote git
ssh-keygen -t rsa -C "e-mail or any other thing that you want here" //Generate a RSA key
cat ~/.ssh/id_rsa.pub  //Find the RSA and paste the output on remote git
~ //This is just to remove the annoying pink line that appears in sublime because of ~ 
-----------------------------------------------------------------------
~~~_*Git tutorial by Corey Schafer*_~~~
https://www.youtube.com/playlist?list=PL-osiE80TeTuRUfjRe54Eea17-YfnOOAx


#check git is installed correctly
git --version 

#set global username and email
git config --global user.name "USERNAME HERE" 
git config --global user.email "EMAIL HERE"

#list all the cinfigration values
git config --list

#to get help for any of the git commands
git <verb> --help

#initialize a repository from the existing code
git init

#before first commit
git status

#add gitignore file and in that file list all the files that you don't want to be on the repository
$>>.gitignore   //you can use this in powershell to create a file or use git bash
#in the gitignore file
<filename 1>
.
.
.
<filename N>

#add files to the staging area
git add -A 					//Add all the files to the staging area
git add <filename> 			//To add a specific file to the staging area
git rm --cached <filename>	//To remove a file from the staging area
or
git reset <filename>		//To remove a file from the staging area
git reset 					//To remove all the files from the staging area

###COMMIT - commit the files in the staging area to the repository
git commit -m "the detailed message describing the information regarding the commit goes here"

#check the log of the commits
git log
git log --stat 	//check the stats related to the commits

#To work on remote directories
git clone <url> <where to clone>

#viewing info about the remote repository
git remote -v
git branch -a

###Pushing the changes, commiting changes to the remote repository.
#make changes to the local repository
git diff		//check the changes that were made
git status		//optional just to check the things
git add -A 		//Add th files to the staging area
or 
git add .
git status		//optional just to check the things
git commit -m "MESSAGE HERE"
#push changes to the remote repository
git pull origin master		//Check that the current version is upto date
git push origin master		//Push the changes to the repository

#branching
git branch <branch name>	//Create a new branch
git checkout <branch name>	//use the branch named
git branch 					//List the name of the branches
#to commit a branch follow the regular commit commands
#push branch to the remote disk
git push -u origin <branch name>	//use simple git pull and push after this
git branch -a 				//get name of the current branches on the remote repository

#merge a branch
git checkout master
git pull origin master or git pull
git branch --merged			//check the name of the branches that have been merged
git merge <branch name>		//merge the specified branch with the master branch
git push origin master	or git push

#delete a branch
git branch --merged 	//check that the brach to be deleted has been merged -> not required
git branch -d <branch name> //delete branch from the local repository
git branch -a 			//check that the branch has been removed from the local repository but not from the remote repository -> not required
git push origin --delete <branch name>  //delete the remote branch






-----------------------------------------------------------------------
###Fixing common git mistakes
#update the message of the last commit
git commit --amend -m "the updated message goes here" 	//amend the message of the last commit ***This changes the update history, be careful when using this***
#add a file that you missed in the last commit
git add <filename> 		//add the file to the staging area
git commit --amend 		//add the files in the staging area to the commit ***This changes the update history, be careful when using this***

#move commit to another branch if you accidentally commited to a wrong branch
git log 	//use this to get the log for the commits and copy the first few characters of the commit hash
git checkout <branch name to which you actually wanted to commit>
git cherry-pick <first few characters of the commit hash that you want to copy> 	//COPY the changes/commit to the correct branch. This just copies to the correct branch it doesn't remove the changes made to the wrong branch

###git resets {soft, mixed (default), hard}
git reset --<reset type> <first few characters of the commit hash that you want to reset to>  
##
{
	soft: go to the specified commit and keep the file modifications that you have in the staging area
	mixed (default): similar to soft reset except the modified files are in the current directory and not the staging area
	hard: remove all the file modifications after the commit execept the untracked files
}
{
	remove the untracked files -> git clean -df 
	-d: remove any untracked directory
	-f: remove any untracked files
	-df: remove untracked directories and files
}
##

###git reflog
git reflog  //shows commits in the order we last commited them

###Return to a state that you removed using reset use git reflog to get the first few characters of the commit hash. This needs to be done within a few days of the reset since these files get permanently deleted after staying in the garbage for few days
git checkout <first few characters of the hash from the git reflog> //this will bring you in a detatched head state
git branch <branch name> 	//this will make a branch for the detached state in the previous step

###make changes without modifying the history
git revert <first few characters of the commit hash that we want to revert (undo changes done by that commit) to> 	//makes a new commit with causing the problems due to change of history

git diff <commit hash1> <commit hash2> 	//check the differences in the files in the two listed commits

###diff merge is better tool to visualize the changes 
#watch one of the videos in the tutorial above for diffMerge